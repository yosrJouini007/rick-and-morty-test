import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  Container,
  Row,
  Col,
} from "reactstrap";

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <Container>
          <Row>
            <Col md="6">
              <h1 className="title">The Rick and Morty API</h1>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}

export default Footer;
