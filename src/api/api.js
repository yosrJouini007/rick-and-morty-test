import axios from 'axios'

export const getAllCharacters = character => {
    return axios
      .get('https://rickandmortyapi.com/api/character', {
        
      })
      .then(response => {
        console.log(response)
        return response.data.results
      })
      .catch(err => {
        console.log(err)
      })
  }
  
export const BASE_URL ='https://rickandmortyapi.com/api/character'