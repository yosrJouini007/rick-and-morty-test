import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";


//import "assets/demo/demo.css";
import "./assets/scss/test-app.scss?v=1.1.0";

import HomePage from "./pages/HomePage";
import DetailsPage from "./pages/DetailsPage";
function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route path="/home" render={props => <HomePage {...props} />} />
      <Route
        path="/details/:characterId"
        render={props => <DetailsPage {...props} />}
      />
      <Redirect from="/" to="/home" />
    </Switch>
  </BrowserRouter>
  );
}

export default App;
