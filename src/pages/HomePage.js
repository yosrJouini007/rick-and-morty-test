import React from "react";

// core components
import IndexNavbar from "../components/NavBar";
import PageHeader from "../components/Header";
import Footer from "../components/Footer";
// reactstrap components
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Input,
  Button,
} from "reactstrap";
import { Link } from 'react-router-dom'
// API
import { getAllCharacters } from '../api/api';



class HomePage extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      characters: [],
      items: [],
      visible: 4,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }
  
  filterList(event) {
		var updatedList = this.state.characters;
		updatedList = updatedList.filter(item =>{
      return item.name.toLowerCase().search(
        event.target.value.toLowerCase()) !== -1;
    });
    this.setState({items: updatedList});
	}

  componentDidMount() {
    document.body.classList.toggle("index-page");
    getAllCharacters()
    .then(res => {
      const characters = res;
      this.setState({ characters });
      this.setState({items: characters})
    })
   
  }
  componentWillUnmount() {
    document.body.classList.toggle("index-page");
  }
  render() {

    return (
      <>
        <IndexNavbar />
        <div className="wrapper">
          <PageHeader />
          <div className="main">
          <div className="mb-3">
          <Col md="4">
             <Input placeholder="Search by name" type="text" onChange={this.filterList.bind(this)} />
             </Col>
              </div>
          <Row>
          { this.state.items.slice(0, this.state.visible).map((character, index)=> {

return (
            <Col className="ml-auto mr-auto" md="3" xl="3" key={character.id}>
           <Link to={`/details/${character.id}`}>
              <Card>
                <CardHeader key={index}>
                <big className="d-block text-uppercase font-weight-bold mb-4">
                {character.name}
                </big>
                </CardHeader>
                <CardBody>
              <Row>
                        <Col md={5}>
             
                <img
                  alt="..."
                  className="img-fluid rounded shadow-lg"
                  src={character.image}
                  style={{ width: "180px" }}
                />
              </Col>  
              <Col className="col align-self-center">
               <Row>
                <span className="d-block text-uppercase font-weight-bold mb-4">
                {character.status}
                </span>
             
              
                </Row>
                <span> • {character.species} </span>
              </Col>          
              </Row>
                </CardBody>
              </Card>
              </Link>
            </Col>
               );
              })}
          </Row>
          {this.state.visible < this.state.items.length &&
             <Button onClick={this.loadMore} type="button"  className="btn-simple btn-round"
             color="primary"
             type="button">Load more</Button>
          }
  
  
          </div>
          <Footer />
        </div>
      </>
    );
  }
}

export default HomePage;