import React from "react";

// core components
import IndexNavbar from "../components/NavBar";
import PageHeader from "../components/Header";
import Footer from "../components/Footer";
// reactstrap components
import {
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  TabContent,
  TabPane,
} from "reactstrap";
//API
import axios from 'axios';
import { BASE_URL } from '../api/api';

class DetailsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            character: {},
            name: '',
            status: '',
            species: '',
            origin: '',
            gender: '',
            location: '',
            image: '',
            tabs:1
        };
      }
      componentDidMount() {
        document.body.classList.toggle("profile-page");
        const { match: { params } } = this.props;

        axios.get(`https://rickandmortyapi.com/api/character/${params.characterId}`)
          .then(res => {
           // console.log('card', card);
           this.setState({ character :res.data});
           this.setState({image:this.state.character.image});
            this.setState({origin:this.state.character.origin.name});
            this.setState({name:this.state.character.name})
            this.setState({status:this.state.character.status})
            this.setState({species:this.state.character.species})
            this.setState({gender:this.state.character.gender})
            this.setState({location:this.state.character.location.name})
          });
      }
      componentWillUnmount() {
        document.body.classList.toggle("profile-page");
      }
      toggleTabs = (e, stateName, index) => {
        e.preventDefault();
        this.setState({
          [stateName]: index
        });
      };
  render() {
    return (
      <>
        <IndexNavbar />
        <div className="wrapper">
            <PageHeader/>
        <Col className="ml-auto mr-auto" lg="4" md="6">
                  <Card className="card-coin card-plain">
                    <CardHeader>
                      <img
                        alt="..."
                        className="img-center img-fluid rounded-circle"
                        src={this.state.image}
                      />
                      <h4 className="title">{this.state.name}</h4>
                    </CardHeader>
                    <CardBody>
                     
                      <TabContent
                        className="tab-subcategories"
                        activeTab={"tab" + this.state.tabs}
                      >
                        <TabPane tabId="tab1">
                          <Table className="tablesorter" >
                            <thead className="text-primary">
                              <tr>
                                <th className="header">Status</th>
                                <th className="header">Species</th>
                                <th className="header">Gender</th>
                               
                         
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>{this.state.status}</td>
                                <td>{this.state.species}</td>
                                <td>{this.state.gender}</td>
                             
                              </tr>
                              <tr>
                              <th className="header"></th>
                              <th className="header">Origin</th>
                              <th className="header"></th>
                              </tr>
                          
                              <tr>
                              <td></td>
                                <td colSpan="2">{this.state.origin}</td>
                                <td></td>
                              </tr>
                              <tr>
                              <th className="header"></th>
                              <th className="header">Location</th>
                              <th className="header"></th>
                              </tr>
                              <tr>
                              <td></td>
                                <td colSpan="2">{this.state.location}</td>
                              </tr>
                            </tbody>
                          </Table>
                        </TabPane>
                       
                      </TabContent>
                    </CardBody>
                  </Card>
                </Col>
          <Footer />
        </div>
      </>
    );
  }
}

export default DetailsPage;